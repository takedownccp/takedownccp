Pod::Spec.new do |spec|
  spec.name         = 'Takedownccp'
  spec.version      = '{{.Version}}'
  spec.license      = { :type => 'GNU Lesser General Public License, Version 3.0' }
  spec.homepage     = 'https://gitlab.com/takedownccp/takedownccp'
  spec.authors      = { {{range .Contributors}}
		'{{.Name}}' => '{{.Email}}',{{end}}
	}
  spec.summary      = 'iOS AnnihilateCCP Client'
  spec.source       = { :git => 'https://gitlab.com/takedownccp/takedownccp.git', :commit => '{{.Commit}}' }

	spec.platform = :ios
  spec.ios.deployment_target  = '9.0'
	spec.ios.vendored_frameworks = 'Frameworks/Takedownccp.framework'

	spec.prepare_command = <<-CMD
    curl https://takedownccpstore.blob.core.windows.net/builds/{{.Archive}}.tar.gz | tar -xvz
    mkdir Frameworks
    mv {{.Archive}}/Takedownccp.framework Frameworks
    rm -rf {{.Archive}}
  CMD
end
