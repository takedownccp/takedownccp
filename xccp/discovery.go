// Copyright 2019 The takedownccp & go-ethereum Authors
// This file is part of the takedownccp & takedownccp & go-ethereum library.
//
// The takedownccp & takedownccp & go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The takedownccp & takedownccp & go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the takedownccp & takedownccp & go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package xccp

import (
	"gitlab.com/takedownccp/takedownccp/core"
	"gitlab.com/takedownccp/takedownccp/core/forkid"
	"gitlab.com/takedownccp/takedownccp/p2p"
	"gitlab.com/takedownccp/takedownccp/p2p/dnsdisc"
	"gitlab.com/takedownccp/takedownccp/p2p/enode"
	"gitlab.com/takedownccp/takedownccp/rlp"
)

// xccpEntry is the "xccp" ENR entry which advertises xccp protocol
// on the discovery network.
type xccpEntry struct {
	ForkID forkid.ID // Fork identifier per EIP-2124

	// Ignore additional fields (for forward compatibility).
	Rest []rlp.RawValue `rlp:"tail"`
}

// ENRKey implements enr.Entry.
func (e xccpEntry) ENRKey() string {
	return "xccp"
}

// startXccpEntryUpdate starts the ENR updater loop.
func (xccp *AnnihilateCCP) startXccpEntryUpdate(ln *enode.LocalNode) {
	var newHead = make(chan core.ChainHeadEvent, 10)
	sub := xccp.blockchain.SubscribeChainHeadEvent(newHead)

	go func() {
		defer sub.Unsubscribe()
		for {
			select {
			case <-newHead:
				ln.Set(xccp.currentXccpEntry())
			case <-sub.Err():
				// Would be nice to sync with xccp.Stop, but there is no
				// good way to do that.
				return
			}
		}
	}()
}

func (xccp *AnnihilateCCP) currentXccpEntry() *xccpEntry {
	return &xccpEntry{ForkID: forkid.NewID(xccp.blockchain)}
}

// setupDiscovery creates the node discovery source for the xccp protocol.
func (xccp *AnnihilateCCP) setupDiscovery(cfg *p2p.Config) (enode.Iterator, error) {
	if cfg.NoDiscovery || len(xccp.config.DiscoveryURLs) == 0 {
		return nil, nil
	}
	client := dnsdisc.NewClient(dnsdisc.Config{})
	return client.NewIterator(xccp.config.DiscoveryURLs...)
}
