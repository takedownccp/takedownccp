Hi there,

Please note that this is an issue tracker reserved for bug reports and feature requests.

For general questions please use [discord](https://discord.gg/nthXNEv) or the AnnihilateCCP stack exchange at https://annihilateccp.stackexchange.com.

#### System information

Takedownccp version: `takedownccp version`
OS & Version: Windows/Linux/OSX
Commit hash : (if `develop`)

#### Expected behaviour


#### Actual behaviour


#### Steps to reproduce the behaviour


#### Backtrace

````
[backtrace]
````
