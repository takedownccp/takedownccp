# This Makefile is meant to be used by people that do not usually work
# with Go source code. If you know what GOPATH is then you probably
# don't need to bother with make.

.PHONY: takedownccp android ios takedownccp-cross evm all test clean
.PHONY: takedownccp-linux takedownccp-linux-386 takedownccp-linux-amd64 takedownccp-linux-mips64 takedownccp-linux-mips64le
.PHONY: takedownccp-linux-arm takedownccp-linux-arm-5 takedownccp-linux-arm-6 takedownccp-linux-arm-7 takedownccp-linux-arm64
.PHONY: takedownccp-darwin takedownccp-darwin-386 takedownccp-darwin-amd64
.PHONY: takedownccp-windows takedownccp-windows-386 takedownccp-windows-amd64

GOBIN = ./build/bin
GO ?= latest
GORUN = env GO111MODULE=on go run

takedownccp:
	$(GORUN) build/ci.go install ./cmd/takedownccp
	@echo "Done building."
	@echo "Run \"$(GOBIN)/takedownccp\" to launch takedownccp."

all:
	$(GORUN) build/ci.go install

android:
	$(GORUN) build/ci.go aar --local
	@echo "Done building."
	@echo "Import \"$(GOBIN)/takedownccp.aar\" to use the library."

ios:
	$(GORUN) build/ci.go xcode --local
	@echo "Done building."
	@echo "Import \"$(GOBIN)/Takedownccp.framework\" to use the library."

test: all
	$(GORUN) build/ci.go test

lint: ## Run linters.
	$(GORUN) build/ci.go lint

clean:
	env GO111MODULE=on go clean -cache
	rm -fr build/_workspace/pkg/ $(GOBIN)/*

# The devtools target installs tools required for 'go generate'.
# You need to put $GOBIN (or $GOPATH/bin) in your PATH to use 'go generate'.

devtools:
	env GOBIN= go get -u golang.org/x/tools/cmd/stringer
	env GOBIN= go get -u github.com/kevinburke/go-bindata/go-bindata
	env GOBIN= go get -u github.com/fjl/gencodec
	env GOBIN= go get -u github.com/golang/protobuf/protoc-gen-go
	env GOBIN= go install ./cmd/abigen
	@type "npm" 2> /dev/null || echo 'Please install node.js and npm'
	@type "solc" 2> /dev/null || echo 'Please install solc'
	@type "protoc" 2> /dev/null || echo 'Please install protoc'

# Cross Compilation Targets (xgo)

takedownccp-cross: takedownccp-linux takedownccp-darwin takedownccp-windows takedownccp-android takedownccp-ios
	@echo "Full cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-*

takedownccp-linux: takedownccp-linux-386 takedownccp-linux-amd64 takedownccp-linux-arm takedownccp-linux-mips64 takedownccp-linux-mips64le
	@echo "Linux cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-*

takedownccp-linux-386:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/386 -v ./cmd/takedownccp
	@echo "Linux 386 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep 386

takedownccp-linux-amd64:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/amd64 -v ./cmd/takedownccp
	@echo "Linux amd64 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep amd64

takedownccp-linux-arm: takedownccp-linux-arm-5 takedownccp-linux-arm-6 takedownccp-linux-arm-7 takedownccp-linux-arm64
	@echo "Linux ARM cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep arm

takedownccp-linux-arm-5:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/arm-5 -v ./cmd/takedownccp
	@echo "Linux ARMv5 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep arm-5

takedownccp-linux-arm-6:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/arm-6 -v ./cmd/takedownccp
	@echo "Linux ARMv6 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep arm-6

takedownccp-linux-arm-7:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/arm-7 -v ./cmd/takedownccp
	@echo "Linux ARMv7 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep arm-7

takedownccp-linux-arm64:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/arm64 -v ./cmd/takedownccp
	@echo "Linux ARM64 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep arm64

takedownccp-linux-mips:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/mips --ldflags '-extldflags "-static"' -v ./cmd/takedownccp
	@echo "Linux MIPS cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep mips

takedownccp-linux-mipsle:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/mipsle --ldflags '-extldflags "-static"' -v ./cmd/takedownccp
	@echo "Linux MIPSle cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep mipsle

takedownccp-linux-mips64:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/mips64 --ldflags '-extldflags "-static"' -v ./cmd/takedownccp
	@echo "Linux MIPS64 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep mips64

takedownccp-linux-mips64le:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=linux/mips64le --ldflags '-extldflags "-static"' -v ./cmd/takedownccp
	@echo "Linux MIPS64le cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-linux-* | grep mips64le

takedownccp-darwin: takedownccp-darwin-386 takedownccp-darwin-amd64
	@echo "Darwin cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-darwin-*

takedownccp-darwin-386:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=darwin/386 -v ./cmd/takedownccp
	@echo "Darwin 386 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-darwin-* | grep 386

takedownccp-darwin-amd64:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=darwin/amd64 -v ./cmd/takedownccp
	@echo "Darwin amd64 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-darwin-* | grep amd64

takedownccp-windows: takedownccp-windows-386 takedownccp-windows-amd64
	@echo "Windows cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-windows-*

takedownccp-windows-386:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=windows/386 -v ./cmd/takedownccp
	@echo "Windows 386 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-windows-* | grep 386

takedownccp-windows-amd64:
	$(GORUN) build/ci.go xgo -- --go=$(GO) --targets=windows/amd64 -v ./cmd/takedownccp
	@echo "Windows amd64 cross compilation done:"
	@ls -ld $(GOBIN)/takedownccp-windows-* | grep amd64
