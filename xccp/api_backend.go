// Copyright 2015 The takedownccp & takedownccp Authors
// This file is part of the takedownccp & go-ethereum library.
//
// The takedownccp & go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The takedownccp & go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the takedownccp & go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

package xccp

import (
	"context"
	"errors"
	"math/big"

	"gitlab.com/takedownccp/takedownccp/accounts"
	"gitlab.com/takedownccp/takedownccp/common"
	"gitlab.com/takedownccp/takedownccp/core"
	"gitlab.com/takedownccp/takedownccp/core/bloombits"
	"gitlab.com/takedownccp/takedownccp/core/rawdb"
	"gitlab.com/takedownccp/takedownccp/core/state"
	"gitlab.com/takedownccp/takedownccp/core/types"
	"gitlab.com/takedownccp/takedownccp/core/vm"
	"gitlab.com/takedownccp/takedownccp/xccp/downloader"
	"gitlab.com/takedownccp/takedownccp/xccp/gasprice"
	"gitlab.com/takedownccp/takedownccp/xccpdb"
	"gitlab.com/takedownccp/takedownccp/event"
	"gitlab.com/takedownccp/takedownccp/params"
	"gitlab.com/takedownccp/takedownccp/rpc"
)

// XccpAPIBackend implements xccpapi.Backend for full nodes
type XccpAPIBackend struct {
	extRPCEnabled bool
	xccp           *AnnihilateCCP
	gpo           *gasprice.Oracle
}

// ChainConfig returns the active chain configuration.
func (b *XccpAPIBackend) ChainConfig() *params.ChainConfig {
	return b.xccp.blockchain.Config()
}

func (b *XccpAPIBackend) CurrentBlock() *types.Block {
	return b.xccp.blockchain.CurrentBlock()
}

func (b *XccpAPIBackend) SetHead(number uint64) {
	b.xccp.protocolManager.downloader.Cancel()
	b.xccp.blockchain.SetHead(number)
}

func (b *XccpAPIBackend) HeaderByNumber(ctx context.Context, number rpc.BlockNumber) (*types.Header, error) {
	// Pending block is only known by the miner
	if number == rpc.PendingBlockNumber {
		block := b.xccp.miner.PendingBlock()
		return block.Header(), nil
	}
	// Otherwise resolve and return the block
	if number == rpc.LatestBlockNumber {
		return b.xccp.blockchain.CurrentBlock().Header(), nil
	}
	return b.xccp.blockchain.GetHeaderByNumber(uint64(number)), nil
}

func (b *XccpAPIBackend) HeaderByNumberOrHash(ctx context.Context, blockNrOrHash rpc.BlockNumberOrHash) (*types.Header, error) {
	if blockNr, ok := blockNrOrHash.Number(); ok {
		return b.HeaderByNumber(ctx, blockNr)
	}
	if hash, ok := blockNrOrHash.Hash(); ok {
		header := b.xccp.blockchain.GetHeaderByHash(hash)
		if header == nil {
			return nil, errors.New("header for hash not found")
		}
		if blockNrOrHash.RequireCanonical && b.xccp.blockchain.GetCanonicalHash(header.Number.Uint64()) != hash {
			return nil, errors.New("hash is not currently canonical")
		}
		return header, nil
	}
	return nil, errors.New("invalid arguments; neither block nor hash specified")
}

func (b *XccpAPIBackend) HeaderByHash(ctx context.Context, hash common.Hash) (*types.Header, error) {
	return b.xccp.blockchain.GetHeaderByHash(hash), nil
}

func (b *XccpAPIBackend) BlockByNumber(ctx context.Context, number rpc.BlockNumber) (*types.Block, error) {
	// Pending block is only known by the miner
	if number == rpc.PendingBlockNumber {
		block := b.xccp.miner.PendingBlock()
		return block, nil
	}
	// Otherwise resolve and return the block
	if number == rpc.LatestBlockNumber {
		return b.xccp.blockchain.CurrentBlock(), nil
	}
	return b.xccp.blockchain.GetBlockByNumber(uint64(number)), nil
}

func (b *XccpAPIBackend) BlockByHash(ctx context.Context, hash common.Hash) (*types.Block, error) {
	return b.xccp.blockchain.GetBlockByHash(hash), nil
}

func (b *XccpAPIBackend) BlockByNumberOrHash(ctx context.Context, blockNrOrHash rpc.BlockNumberOrHash) (*types.Block, error) {
	if blockNr, ok := blockNrOrHash.Number(); ok {
		return b.BlockByNumber(ctx, blockNr)
	}
	if hash, ok := blockNrOrHash.Hash(); ok {
		header := b.xccp.blockchain.GetHeaderByHash(hash)
		if header == nil {
			return nil, errors.New("header for hash not found")
		}
		if blockNrOrHash.RequireCanonical && b.xccp.blockchain.GetCanonicalHash(header.Number.Uint64()) != hash {
			return nil, errors.New("hash is not currently canonical")
		}
		block := b.xccp.blockchain.GetBlock(hash, header.Number.Uint64())
		if block == nil {
			return nil, errors.New("header found, but block body is missing")
		}
		return block, nil
	}
	return nil, errors.New("invalid arguments; neither block nor hash specified")
}

func (b *XccpAPIBackend) StateAndHeaderByNumber(ctx context.Context, number rpc.BlockNumber) (*state.StateDB, *types.Header, error) {
	// Pending state is only known by the miner
	if number == rpc.PendingBlockNumber {
		block, state := b.xccp.miner.Pending()
		return state, block.Header(), nil
	}
	// Otherwise resolve the block number and return its state
	header, err := b.HeaderByNumber(ctx, number)
	if err != nil {
		return nil, nil, err
	}
	if header == nil {
		return nil, nil, errors.New("header not found")
	}
	stateDb, err := b.xccp.BlockChain().StateAt(header.Root)
	return stateDb, header, err
}

func (b *XccpAPIBackend) StateAndHeaderByNumberOrHash(ctx context.Context, blockNrOrHash rpc.BlockNumberOrHash) (*state.StateDB, *types.Header, error) {
	if blockNr, ok := blockNrOrHash.Number(); ok {
		return b.StateAndHeaderByNumber(ctx, blockNr)
	}
	if hash, ok := blockNrOrHash.Hash(); ok {
		header, err := b.HeaderByHash(ctx, hash)
		if err != nil {
			return nil, nil, err
		}
		if header == nil {
			return nil, nil, errors.New("header for hash not found")
		}
		if blockNrOrHash.RequireCanonical && b.xccp.blockchain.GetCanonicalHash(header.Number.Uint64()) != hash {
			return nil, nil, errors.New("hash is not currently canonical")
		}
		stateDb, err := b.xccp.BlockChain().StateAt(header.Root)
		return stateDb, header, err
	}
	return nil, nil, errors.New("invalid arguments; neither block nor hash specified")
}

func (b *XccpAPIBackend) GetReceipts(ctx context.Context, hash common.Hash) (types.Receipts, error) {
	return b.xccp.blockchain.GetReceiptsByHash(hash), nil
}

func (b *XccpAPIBackend) GetLogs(ctx context.Context, hash common.Hash) ([][]*types.Log, error) {
	receipts := b.xccp.blockchain.GetReceiptsByHash(hash)
	if receipts == nil {
		return nil, nil
	}
	logs := make([][]*types.Log, len(receipts))
	for i, receipt := range receipts {
		logs[i] = receipt.Logs
	}
	return logs, nil
}

func (b *XccpAPIBackend) GetTd(ctx context.Context, hash common.Hash) *big.Int {
	return b.xccp.blockchain.GetTdByHash(hash)
}

func (b *XccpAPIBackend) GetEVM(ctx context.Context, msg core.Message, state *state.StateDB, header *types.Header) (*vm.EVM, func() error, error) {
	vmError := func() error { return nil }

	context := core.NewEVMContext(msg, header, b.xccp.BlockChain(), nil)
	return vm.NewEVM(context, state, b.xccp.blockchain.Config(), *b.xccp.blockchain.GetVMConfig()), vmError, nil
}

func (b *XccpAPIBackend) SubscribeRemovedLogsEvent(ch chan<- core.RemovedLogsEvent) event.Subscription {
	return b.xccp.BlockChain().SubscribeRemovedLogsEvent(ch)
}

func (b *XccpAPIBackend) SubscribePendingLogsEvent(ch chan<- []*types.Log) event.Subscription {
	return b.xccp.miner.SubscribePendingLogs(ch)
}

func (b *XccpAPIBackend) SubscribeChainEvent(ch chan<- core.ChainEvent) event.Subscription {
	return b.xccp.BlockChain().SubscribeChainEvent(ch)
}

func (b *XccpAPIBackend) SubscribeChainHeadEvent(ch chan<- core.ChainHeadEvent) event.Subscription {
	return b.xccp.BlockChain().SubscribeChainHeadEvent(ch)
}

func (b *XccpAPIBackend) SubscribeChainSideEvent(ch chan<- core.ChainSideEvent) event.Subscription {
	return b.xccp.BlockChain().SubscribeChainSideEvent(ch)
}

func (b *XccpAPIBackend) SubscribeLogsEvent(ch chan<- []*types.Log) event.Subscription {
	return b.xccp.BlockChain().SubscribeLogsEvent(ch)
}

func (b *XccpAPIBackend) SendTx(ctx context.Context, signedTx *types.Transaction) error {
	return b.xccp.txPool.AddLocal(signedTx)
}

func (b *XccpAPIBackend) GetPoolTransactions() (types.Transactions, error) {
	pending, err := b.xccp.txPool.Pending()
	if err != nil {
		return nil, err
	}
	var txs types.Transactions
	for _, batch := range pending {
		txs = append(txs, batch...)
	}
	return txs, nil
}

func (b *XccpAPIBackend) GetPoolTransaction(hash common.Hash) *types.Transaction {
	return b.xccp.txPool.Get(hash)
}

func (b *XccpAPIBackend) GetTransaction(ctx context.Context, txHash common.Hash) (*types.Transaction, common.Hash, uint64, uint64, error) {
	tx, blockHash, blockNumber, index := rawdb.ReadTransaction(b.xccp.ChainDb(), txHash)
	return tx, blockHash, blockNumber, index, nil
}

func (b *XccpAPIBackend) GetPoolNonce(ctx context.Context, addr common.Address) (uint64, error) {
	return b.xccp.txPool.Nonce(addr), nil
}

func (b *XccpAPIBackend) Stats() (pending int, queued int) {
	return b.xccp.txPool.Stats()
}

func (b *XccpAPIBackend) TxPoolContent() (map[common.Address]types.Transactions, map[common.Address]types.Transactions) {
	return b.xccp.TxPool().Content()
}

func (b *XccpAPIBackend) SubscribeNewTxsEvent(ch chan<- core.NewTxsEvent) event.Subscription {
	return b.xccp.TxPool().SubscribeNewTxsEvent(ch)
}

func (b *XccpAPIBackend) Downloader() *downloader.Downloader {
	return b.xccp.Downloader()
}

func (b *XccpAPIBackend) ProtocolVersion() int {
	return b.xccp.XccpVersion()
}

func (b *XccpAPIBackend) SuggestPrice(ctx context.Context) (*big.Int, error) {
	return b.gpo.SuggestPrice(ctx)
}

func (b *XccpAPIBackend) ChainDb() xccpdb.Database {
	return b.xccp.ChainDb()
}

func (b *XccpAPIBackend) EventMux() *event.TypeMux {
	return b.xccp.EventMux()
}

func (b *XccpAPIBackend) AccountManager() *accounts.Manager {
	return b.xccp.AccountManager()
}

func (b *XccpAPIBackend) ExtRPCEnabled() bool {
	return b.extRPCEnabled
}

func (b *XccpAPIBackend) RPCGasCap() uint64 {
	return b.xccp.config.RPCGasCap
}

func (b *XccpAPIBackend) RPCTxFeeCap() float64 {
	return b.xccp.config.RPCTxFeeCap
}

func (b *XccpAPIBackend) BloomStatus() (uint64, uint64) {
	sections, _, _ := b.xccp.bloomIndexer.Sections()
	return params.BloomBitsBlocks, sections
}

func (b *XccpAPIBackend) ServiceFilter(ctx context.Context, session *bloombits.MatcherSession) {
	for i := 0; i < bloomFilterThreads; i++ {
		go session.Multiplex(bloomRetrievalBatch, bloomRetrievalWait, b.xccp.bloomRequests)
	}
}
