// Copyright 2015 The takedownccp & takedownccp & go-ethereum Authors
// This file is part of the takedownccp & takedownccp & go-ethereum library.
//
// The takedownccp & takedownccp & go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The takedownccp & takedownccp & go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the takedownccp & takedownccp & go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

// Contains the metrics collected by the downloader.

package downloader

import (
	"gitlab.com/takedownccp/takedownccp/metrics"
)

var (
	headerInMeter      = metrics.NewRegisteredMeter("xccp/downloader/headers/in", nil)
	headerReqTimer     = metrics.NewRegisteredTimer("xccp/downloader/headers/req", nil)
	headerDropMeter    = metrics.NewRegisteredMeter("xccp/downloader/headers/drop", nil)
	headerTimeoutMeter = metrics.NewRegisteredMeter("xccp/downloader/headers/timeout", nil)

	bodyInMeter      = metrics.NewRegisteredMeter("xccp/downloader/bodies/in", nil)
	bodyReqTimer     = metrics.NewRegisteredTimer("xccp/downloader/bodies/req", nil)
	bodyDropMeter    = metrics.NewRegisteredMeter("xccp/downloader/bodies/drop", nil)
	bodyTimeoutMeter = metrics.NewRegisteredMeter("xccp/downloader/bodies/timeout", nil)

	receiptInMeter      = metrics.NewRegisteredMeter("xccp/downloader/receipts/in", nil)
	receiptReqTimer     = metrics.NewRegisteredTimer("xccp/downloader/receipts/req", nil)
	receiptDropMeter    = metrics.NewRegisteredMeter("xccp/downloader/receipts/drop", nil)
	receiptTimeoutMeter = metrics.NewRegisteredMeter("xccp/downloader/receipts/timeout", nil)

	stateInMeter   = metrics.NewRegisteredMeter("xccp/downloader/states/in", nil)
	stateDropMeter = metrics.NewRegisteredMeter("xccp/downloader/states/drop", nil)

	throttleCounter = metrics.NewRegisteredCounter("xccp/downloader/throttle", nil)
)
